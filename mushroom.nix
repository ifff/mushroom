{ mkDerivation, base, bytestring, containers, data-fix, filepath
, haskeline, hpack, lens, mtl, parsec, parsec3-numbers, prettify
, stdenv, text
}:
mkDerivation {
  pname = "mushroom";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base bytestring containers data-fix filepath haskeline lens mtl
    parsec parsec3-numbers prettify text
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base bytestring containers data-fix filepath haskeline lens mtl
    parsec parsec3-numbers prettify text
  ];
  testHaskellDepends = [
    base bytestring containers data-fix filepath haskeline lens mtl
    parsec parsec3-numbers prettify text
  ];
  prePatch = "hpack";
  description = "A functional programming language with an advanced type system";
  license = stdenv.lib.licenses.bsd3;
}
