{-# LANGUAGE FlexibleContexts #-}

module MushroomRunner where

import Control.Exception (IOException, try)
import Control.Monad (when)
import Control.Monad.Except
import Control.Monad.State

import Data.Fix
import Data.List (isPrefixOf)
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import qualified Data.Set as S

import Debug.Trace (trace)

import System.Console.Haskeline
import System.FilePath
import System.IO (FilePath, readFile)

-- import LLVM.Pretty

-- import Mushroom.Compiler.Compiler
import Mushroom.Interpreter.Interpreter
import Mushroom.Interpreter.Util
import Mushroom.Parser.Parser
import Mushroom.Preproc.MacroExpand
import Mushroom.PrettyPrint
import Mushroom.Typechecker.Typechecker
import Mushroom.Typechecker.Util
import Mushroom.Types

-- | The CLI for running a Mushroom program, either from a file or through a REPL.
-- Expects a list of command-line arguments (which are passed via `main' in app/Main.hs)
runMushroom :: [String] -> IO ()
runMushroom ["repl"] = runRepl
runMushroom ["run", fpath] = runFile fpath >> return ()
runMushroom [] = putStrLn "Expected arguments, but given none. Aborting."
runMushroom _ = putStrLn "Unexpected argument(s). Aborting."

runRepl :: IO ()
runRepl = do
  putStrLn $ "Welcome to the Mushroom REPL! Enter :q to quit."
  putStrLn "\n"
  runRepl' newProgramState

-- | Run a Mushroom program through a REPL.
runRepl' :: ProgramState -> IO ()
runRepl' state = do
  -- read input
  input <- runInputT defaultSettings readline
  -- eval, print and loop unless user used :q to exit;
  unless (input == ":q") $ do
    state' <- runProgram' state input
    runRepl' state'
  where readline :: InputT IO String
        readline = do
          minput <- getInputLine "> "
          case minput of
            Nothing -> return ""
            Just input -> return input

-- | Run a Mushroom program from a file, discarding the results.
runFile :: FilePath -> IO ()
runFile fpath = evalFile fpath >> return ()

-- | Similar to @runFile@, but returns the state after evaluating the program so that the results
-- can be 'loaded' into a subsequent execution function.
evalFile :: FilePath -> IO ProgramState
evalFile fpath = do
  program <- try (readFile fpath) :: IO (Either IOException String)
  case program of
    Right p -> runProgram p
    Left e -> putStrLn ("Error: " ++ show e) >> return newProgramState

-- | Parse a string representing a Mushroom program, run it, and display the results.
runProgram :: String -> IO ProgramState
runProgram = runProgram' newProgramState

-- | Parse a string representing a Mushroom program, run it, and display the results.
-- This version of the function takes program state as an additional parameter.
runProgram' :: ProgramState -> String -> IO ProgramState
runProgram' state program = do
  -- try parsing the program
  let tokens = parseMushroom program
  -- display error if there were issues parsing; otherwise, carry on
  case tokens of
    Left err -> putStrLn ("Parse Error: " ++ show err) >> return state
    Right (imports, terms) -> do
      -- get the state from the imported files
      importState <- handleImports imports
      let state' = mergeStates $
            (\(a1, a2, a3) (b1, b2, b3) -> ([a1, b1], [a2, b2], [a3, b3])) state importState
      processTerms state' terms

-- | evaluate all the imported modules and return their end states all combined into one.
handleImports :: [FilePath] -> IO ProgramState
handleImports paths = (mergeStates . unzip3) `fmap` mapM evalFile paths
  where unzip3 xs = (map fst' xs, map snd' xs, map third xs)
        fst' (a, _, _) = a
        snd' (_, b, _) = b
        third (_, _, c) = c

-- | Merge program states together into a single state.
mergeStates :: ([PreprocState], [TypecheckState], [InterpreterState]) -> ProgramState
mergeStates (pStates, tyStates, inStates) = (pState, tyState, inState)
  where pState = foldr (\st1 st2 -> newPreprocState
                         { _macros = _macros st1 `withCtx` _macros st2})
                 newPreprocState pStates
        tyState = foldr (\st1 st2 -> newTypecheckState 
                          { _vars = [head (_vars st1) `withCtx` head (_vars st2)]
                          , _typeVars = [head (_typeVars st1) `withCtx` head (_typeVars st2)]
                          , _typeVarCount = _typeVarCount st1 `max` _typeVarCount st2
                          , _tagMap = [head (_tagMap st1) `withCtx` head (_tagMap st2)] })
                  newTypecheckState tyStates
        inState = foldr (\st1 st2 -> InterpreterState
                          [last (_bindings st1) `withCtx` last (_bindings st2)])
                          newInterpreterState inStates

-- | Given a program state, try to run a program through the interpreter and typechecker,
-- returning the new program state if successful and returning the old state if an error occurs.
processTerms :: ProgramState -> [Term] -> IO ProgramState
processTerms st@(pState, tyState, inState) terms = do
 -- put this back in when you're ready to use the preprocessor again:
 -- let presults = runPreprocessor pState terms
 -- case presults of
 --   Left err -> quit err
 --   Right (pState', terms') -> do
  let typeResults = runTypechecker tyState terms -- terms'
  case typeResults of
    Left err -> quit err
    Right (tyState', typedTerms) -> do
      evalResults <- runInterpreter inState typedTerms
      case evalResults of
        Left err -> quit err
        Right (inState', results) -> do
          mapM printTerm results
          return (newPreprocState {- pState' -}, tyState', inState')
  where quit err = print err >> return st

-- -- | Compiles a program into LLVM IR. This is only meant for simple testing/debugging.
-- compileIR code = case parseMushroom code of
--   Left err -> error (show err)
--   Right (_, terms) ->
--     case runTypechecker newTypecheckState terms of
--       Left err -> error (show err)
--       -- this is made with the rash assumption that the roots of the AST
--       -- are exclusively definitions (so fix this later)
--       Right (_, typedTerms) ->
--         case runCompiler newCompilerState typedTerms of
--           Right st -> ppllvm (assembleModule st)
--           Left err -> error (show err)

-- | Prints a typed term's string representation.
-- If the term is Term'None, don't bother printing it.
printTerm :: TypedTerm -> IO ()
printTerm Fix{unFix = Term'None `Typed` _} = return ()
printTerm Fix{unFix = _ `Typed` Type'None} = return ()
printTerm Fix{unFix = t `Typed` ty} = putStrLn (prettyPrint t ++ " : " ++ prettyPrint ty)
