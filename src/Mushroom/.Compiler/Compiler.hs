{-# LANGUAGE OverloadedStrings #-}

module Mushroom.Compiler.Compiler where

import Prelude hiding (div)

import Control.Arrow (second)
import Control.Lens
import Control.Monad (forM_, sequence)
import Control.Monad.Except (runExceptT)
import Control.Monad.State (runState)

import Data.Fix
import Data.List (mapAccumL)
import Data.Tuple (swap)
import qualified Data.Map.Strict as M
import qualified Data.Text as T (unpack, length)

import Debug.Trace (trace)

import LLVM.AST hiding (Type)
import qualified LLVM.AST as AST
import LLVM.AST.Constant
import LLVM.AST.Instruction as Instr
import LLVM.AST.Operand
import LLVM.AST.Type hiding (Type)

import Mushroom.Compiler.Instructions
import Mushroom.Compiler.Util
import Mushroom.PrettyPrint
import Mushroom.Types

{-

COMPILATION CHECKLIST
---------------------
Add compilation rules and boilerplate in the incremental steps below.
Keep in mind that all types are assumed to be concrete before the 'polymorphism' step.

* [x] int
* [x] str
* [x] list
* [x] product

* [x] numeric builtins
| Watch out for division by zero
* [x] abs

* [ ] other builtins
| This would need extern functions (for printing) and more LLVM ops for the list stuff

* [ ] match
| This would require lots of `br` kludges and blocks
* [ ] variant
| Variant values could probably just be structs with an extra first value that represents their
| tag numerically or something
* [ ] record
| For records you'd need to generate the special access functions.
| There's a builtin LLVM instruction for accessing struct fields, so you don't
| have to mess with matching hacks this time.

* [ ] variable references/decls (local and global)

* [ ] anonymous/first-class fn
| Probably need to be defined globally somehow, with a unique name.
| You also would need to figure out function pointers.
* [ ] closure
| This is apparently pretty complex (i.e., do research) but you'll need
| to play around with structs as arguments or something (???)

* [ ] fiber
| The challenge here would be internal state, and it'll prob be the first case where you'd
| need to use more than just an entry block (maybe add label arguments?)

* [ ] polymorphism

- also add in a JIT runner

-}

-- | Assembles an LLVM module from the compiler's state.
assembleModule :: CompilerState -> Module
assembleModule st = AST.Module
  { moduleName = "foo"
  , moduleSourceFileName = "foo"
  , moduleDataLayout = Nothing
  , moduleTargetTriple = Nothing
  , moduleDefinitions = st^.defs }

runCompiler :: CompilerState -> [TypedTerm] -> Either MushroomError CompilerState
runCompiler cState terms =
  case result of
    (st, Left err) -> Left err
    (st, Right _) -> Right st
  where go c t = swap $ runState (runExceptT . runCompile $ compileDef t) c
        result = second sequence (mapAccumL go cState terms)

-- | Compiles a definition into a representation of LLVM IR and stores it into the @CompileState@
-- record.
compileDef :: TypedTerm -> MushroomCompile ()
compileDef Fix{unFix = Term'Define name
                -- TODO: handle abstract types
                abs@Fix{unFix = Term'Abs ps env body `Typed` Type'Abs ptys rty} `Typed` _} = do
  -- compile the body into operands, which will create some local variables
  operands <- mapM compileOperand body
  instrs <- use localDecls
  -- get the parameter and return types
  -- TODO: find a way to incorporate arbitrary list arguments
  ptys' <- mapM compileDummyTy ptys
  retty <- compileDummyTy rty
  -- TODO: expand block creation helpers once you implement matching
  let ret = Do $ Instr.Ret (Just (last operands)) []
      bl = mkBlock "entry" instrs ret
      params = zip ps ptys'
  defn retty name params [bl]
  -- clean out the local declarations
  localDecls .= []
compileDef Fix{unFix = Term'Define name val `Typed` _} = do
  -- get an LLVM representation of the variable's value and type
  val' <- compileConst val
  ty <- compileTy val
  -- define the variable
  defvar name ty val'
compileDef Fix{unFix = Term'RcdDecl _ fields `Typed` _} = do
  fieldTys <- mapM compileDummyTy (map snd fields)
  let rcdTy = StructureType False fieldTys
      fields' = zip [0..] fields -- index each field
  -- For every field, declare a function to access it.
  forM_ fields' $ \(fnum, (fname, fty)) -> do
    -- the name of the sole parameter (a record)
    let paramName = "rcd"
    -- the record's type
    retty <- compileDummyTy fty
    -- a reference to the fnum'th field of the record
    ref <- getElems [fnum] (LocalReference rcdTy (symToName paramName))
    instrs <- use localDecls
    localDecls .= []
    let param = (paramName, rcdTy)
        ret = Do $ Instr.Ret (Just ref) []
        bl = mkBlock "entry" instrs ret
    defn retty fname [param] [bl]
compileDef Fix{unFix = Term'VarDecl _ _ `Typed` _} = return ()
compileDef t = compileError ("Expected definition, but got value instead: " ++ prettyPrint t)

-- | Converts a value (e.g. a string, int, collection, etc) into an LLVM constant.
compileConst :: TypedTerm -> MushroomCompile Constant
compileConst Fix{unFix = Term'IntLit i `Typed` _} = return $
  Int 32 (fromIntegral i)
compileConst Fix{unFix = Term'StrLit s `Typed` _} = return $
  Array i8 (map charToInt $ T.unpack s)
compileConst list@Fix{unFix = Term'List xs `Typed` _} = do
  ArrayType _ elemTy <- compileTy list
  xs' <- mapM compileConst xs
  return (Array elemTy xs')
compileConst Fix{unFix = Term'Product xs `Typed` _} = do
  xs' <- mapM compileConst xs
  return (Struct Nothing False xs')
compileConst Fix{unFix = Term'Variant name ps `Typed` ty} = do
  ps' <- mapM compileConst ps
  return (Struct (Just $ symToName name) False ps')
compileConst Fix{unFix = Term'Record name fields `Typed` _} = do
  fields' <- mapM compileConst (M.elems fields)
  return (Struct (Just $ symToName name) False fields')
compileConst t = compileError (prettyPrint t ++ " could not be compiled into a value.")

-- | Compiles a term into an operand.
compileOperand :: TypedTerm -> MushroomCompile Operand
compileOperand t@Fix{unFix = Term'AppAbs Fix{unFix = Term'Builtin b `Typed` _} args `Typed` _} =
  mapM compileOperand args >>= \args' ->
  case (b, args') of
    (Builtin'Add, [x, y]) -> add x y
    (Builtin'Sub, [x, y]) -> sub x y
    (Builtin'Mul, [x, y]) -> mul x y
    (Builtin'Div, [x, y]) -> div x y
    (Builtin'Hd, [xs]) -> hd xs
compileOperand t@Fix{unFix = Term'Var name `Typed` ty} = do
  let ident = symToName name
  ty <- compileDummyTy ty
  return (LocalReference ty ident)
compileOperand t = ConstantOperand `fmap` compileConst t
-- compileOperand t = compileError (prettyPrint t ++ " could not be compiled into an operand")

-- | Computes the LLVM type of a @TypedTerm@.
compileTy :: TypedTerm -> MushroomCompile AST.Type
compileTy Fix{unFix = _ `Typed` Type'Int} = return i32
compileTy Fix{unFix = Term'StrLit s `Typed` Type'Str} = return $
  ArrayType (fromIntegral $ T.length s) i8
compileTy Fix{unFix = Term'List xs `Typed` ty} = do
  elemTy <- if null xs then return i8 else compileTy (head xs)
  return $ ArrayType (fromIntegral $ length xs) elemTy
compileTy Fix{unFix = Term'Product xs `Typed` Type'Product _} =
  StructureType False `fmap` mapM compileTy xs
compileTy Fix{unFix = Term'Variant name ps `Typed` _} =
  StructureType False `fmap` mapM compileTy ps
compileTy Fix{unFix = Term'Record name fields `Typed` _} =
  StructureType False `fmap` mapM compileTy (M.elems fields)
compileTy t = compileError ("The type of " ++ prettyPrint t ++ " could not be compiled")

-- | Converts a Mushroom type into an LLVM type. It pairs the type with a dummy term
-- (@Term'None@), so you shouldn't use this when the term is meaningful to the type
-- (for example, lists and strings require length for the type in LLVM).
compileDummyTy :: Type -> MushroomCompile AST.Type
compileDummyTy ty = compileTy Fix{unFix = Term'None `Typed` ty}
