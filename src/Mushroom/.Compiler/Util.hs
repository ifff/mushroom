{-# LANGUAGE OverloadedStrings #-}

module Mushroom.Compiler.Util where

import Control.Monad.Except
import Control.Lens

import Data.Char (ord)
import qualified Data.ByteString.Char8 as C (pack)
import qualified Data.ByteString.Short as S (toShort)

import LLVM.AST hiding (Type)
import qualified LLVM.AST as AST (Type)
import LLVM.AST.Constant
import LLVM.AST.Global

import Mushroom.Types

compileError :: String -> MushroomCompile a
compileError = throwError . newMushroomError CompileError

-- | Helper function for creating a @BasicBlock@.
mkBlock :: Symbol -> [Named Instruction] -> Named Terminator -> BasicBlock
mkBlock name = BasicBlock (symToName name)

-- | Defines a function globally.
defn :: AST.Type -> Symbol -> [(Symbol, AST.Type)] -> [BasicBlock] -> MushroomCompile ()
defn retty fname params blocks = addGlobal (mkFn retty fname params blocks)

-- | Defines a global variable.
defvar :: Symbol -> AST.Type -> Constant -> MushroomCompile ()
defvar vname ty val = addGlobal (mkVar vname ty val)

-- | Adds a local definition by assigning a unique placeholder name to an instruction.
addLocal :: Instruction -> MushroomCompile ()
addLocal val = do
  -- create a unique numerical name for the local variable
  n <- use localCount
  let name = UnName (fromIntegral n)
      decl = name := val
  -- add the declaration to the list
  localDecls %= (++ [decl])
  -- update the count of local variables
  localCount += 1

-- | Adds a global definition.
addGlobal :: Global -> MushroomCompile ()
addGlobal = addDefinition . GlobalDefinition

-- | Adds a definition to the state.
addDefinition :: Definition -> MushroomCompile ()
addDefinition def = defs %= (++[def])

-- | Constructs a global variable given a name, type, and value.
mkVar :: Symbol -> AST.Type -> Constant -> Global
mkVar vname ty val = globalVariableDefaults
  { name = symToName vname
  , LLVM.AST.Global.type' = ty
  , initializer = Just val }

-- | Constructs a global function given a return type, name, and parameter list.
mkFn :: AST.Type -> Symbol -> [(Symbol, AST.Type)] -> [BasicBlock] -> Global
mkFn retty fname params blocks = functionDefaults
  { returnType = retty
  , name = symToName fname
  , parameters = (map mkParams params, False {- False = no varargs -})
  , basicBlocks = blocks }
  where mkParams (str, ty) = Parameter ty (symToName str) []

-- | Converts a symbol into a name.
symToName :: Symbol -> Name
symToName = Name . S.toShort . C.pack

-- | Converts a char to an integer value compatible with LLVM.
charToInt :: Char -> Constant
charToInt c = Int 8 (fromIntegral $ ord c)
