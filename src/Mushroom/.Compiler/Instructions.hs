-- | Some helper functions that wrap LLVM instructions.
module Mushroom.Compiler.Instructions where

import Control.Lens

import LLVM.AST.Instruction as Instr
import LLVM.AST.Name
import LLVM.AST.Operand
import LLVM.AST.Type hiding (Type)

import Mushroom.Compiler.Util
import Mushroom.Types

-- | Binds the invocation of an instruction to a name and returns the reference to that name.
bindInstr :: Instruction -> MushroomCompile Operand
bindInstr inst = do
  ident <- uses localCount (UnName . fromIntegral)
  addLocal inst
  -- TODO: *actually* determine the reference type here
  return (LocalReference i32 ident)

add :: Operand -> Operand -> MushroomCompile Operand
add x y = bindInstr $ Instr.Add False False x y []

sub :: Operand -> Operand -> MushroomCompile Operand
sub x y = bindInstr $ Instr.Sub False False x y []

mul :: Operand -> Operand -> MushroomCompile Operand
mul x y = bindInstr $ Instr.Mul False False x y []

div :: Operand -> Operand -> MushroomCompile Operand
div x y = bindInstr $ Instr.SDiv False x y []

hd :: Operand -> MushroomCompile Operand
hd = getElems [0]

getElems :: [Int] -> Operand -> MushroomCompile Operand
getElems ns xs = bindInstr $ Instr.ExtractValue xs (map fromIntegral ns) []

-- TODO: print, list ops
