module Mushroom.Interpreter.Util
  ( addVar
  , runtimeError
  , pushBindings
  , popBindings
  , withTempBindings ) where

import Control.Lens
import Control.Monad.Except
import qualified Data.Map.Strict as M

import Mushroom.Types

addVar :: Symbol -> TypedTerm -> MushroomEval ()
addVar sym val = bindings %= \(a:as) -> (M.insert sym val a):as

runtimeError :: String -> MushroomEval a
runtimeError = throwError . newMushroomError RuntimeError

pushBindings :: CtxMap TypedTerm -> MushroomEval ()
pushBindings newBindings = do
  oldBindings <- uses bindings head
  let bindings' = oldBindings `withCtx` newBindings
  bindings %= (bindings':)

popBindings :: MushroomEval BindingMap
popBindings = do
  vars <- uses bindings head
  bindings %= tail
  return vars

withTempBindings :: MushroomEval a -> BindingMap -> MushroomEval a
withTempBindings f bindings = do
  pushBindings bindings
  result <- f
  popBindings
  return result
