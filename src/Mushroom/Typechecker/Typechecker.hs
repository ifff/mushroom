module Mushroom.Typechecker.Typechecker (typecheck, runTypechecker) where

import Control.Arrow
import Control.Lens
import Control.Monad
import Control.Monad.Except
import Control.Monad.State

import Data.List ((\\), intercalate, mapAccumL)
import Data.Fix
import Data.Tuple (swap)
import qualified Data.Set as S
import qualified Data.Map.Strict as M

import Debug.Trace (trace)

import Mushroom.PrettyPrint
import Mushroom.Types
import Mushroom.Typechecker.Unify
import Mushroom.Typechecker.Util

-- | Run the typechecking algorithm on a list of successive @Terms@ provided a @TypecheckState@,
-- yielding an error if something goes wrong, and otherwise returning an updated @TypecheckState@
-- and a list of @Types@ matching the @Terms@.
runTypechecker :: TypecheckState -> [Term] -> Either MushroomError (TypecheckState, [TypedTerm])
runTypechecker tcState terms =
  case sequence $ snd results of
    Left msg -> Left msg
    Right terms' -> Right (fst results, terms')
  where results = mapAccumL go tcState terms
        go c t = swap $ runState (runExceptT . runTypecheck $ typecheck t) c

-- | Typechecks a term and all its subterms, returning a typed version of that term.
typecheck :: Term -> MushroomTypecheck TypedTerm

-- int literals have an obvious type
typecheck Fix{unFix = Term'IntLit x} = return Fix{unFix = Term'IntLit x `Typed` Type'Int}

-- so do float literals
typecheck Fix{unFix = Term'FloatLit x} = return Fix{unFix = Term'FloatLit x `Typed` Type'Float}

-- so do string literals
typecheck Fix{unFix = Term'StrLit s} = return Fix{unFix = Term'StrLit s `Typed` Type'Str}

-- so do char literals
typecheck Fix{unFix = Term'CharLit c} = return Fix{unFix = Term'CharLit c `Typed` Type'Char}

typecheck Fix{unFix = Term'ModuleDecl modName termExports typeExports defs} = do
  -- create a new context in which to capture the new variables and tags
  pushCtx vars M.empty
  pushCtx typeVars M.empty
  pushCtx tagMap M.empty
  -- add the module name to the prefix list
  enterModule modName
  -- typecheck the definitions
  defs' <- mapM typecheck defs
  -- collect the variable contexts in the module, which hold all the new bindings
  modVars <- M.toList `fmap` popCtx vars vars
  modTypes <- M.toList `fmap` popCtx typeVars typeVars
  modTags <- M.toList `fmap` popCtx tagMap tagMap
  -- update the export names so they have the right prefixes, i.e., they match the names
  -- of their corresponding variables/types
  termExports' <- mapM addPrefix termExports
  typeExports' <- mapM addPrefix typeExports
  exitModule
  -- filter these contexts to contain only the exported names, and then put a module prefix on
  -- the front of those names
  let modVars' = filterCtx termExports' modVars
      modTypes' = filterCtx typeExports' modTypes
      modTags' = filterTags typeExports' modTags
  -- check to see if there were exported names that weren't in the module's definitions
  let undefinedVars = termExports' \\ map fst modVars
      undefinedTypes = typeExports' \\ map fst modTypes
  unless (null undefinedVars) $
    typeError ("Cannot export undefined variables: " ++ intercalate ", " undefinedVars)
  unless (null undefinedTypes) $
    typeError ("Cannot export undefined types: " ++ intercalate ", " undefinedTypes)
  -- add the exported contexts into the current context
  mapM_ (uncurry addVar) modVars'
  mapM_ (uncurry addTypeVar) modTypes'
  mapM_ (uncurry addTag) modTags'
  return Fix{unFix = Term'ModuleDecl modName termExports typeExports defs' `Typed` Type'None}
  where filterCtx exportList = filter (\(k, v) -> k `elem` exportList)
        filterTags exportList = filter (tagIsExported exportList)
        tagIsExported exportList (_, ty) =
          let name = case ty of
                       Type'Variant n _ -> n
                       Type'TypeAbs _ (Type'Variant n _) -> n
          in name `elem` exportList

-- typecheck a variable binding, add it to the context, and then generalize its type
typecheck Fix{unFix = Term'Define sym t} = do
  x <- freshTypeVar >>= generalize
  addVar sym x
  (t', ty) <- typecheck' t
  addVar sym ty
  subs <- use substitutions
  vars %= \v -> v & ix 0 %~ M.map (sub subs)
  ty' <- generalize ty
  addVar sym ty'
  return Fix{unFix = Term'Define sym t' `Typed` Type'None}

-- typecheck a variant declaration by adding its tags to the tag map and adding
-- its name to the typevar map
typecheck Fix{unFix = Term'VarDecl name tags} = do
  -- TODO: is there a way to condense all these mapMs?
  -- (maybe use a lens????)
  tags' <- mapM (mapM $ mapM appTy) tags
  let ty = Type'Variant name (M.fromList tags')
  ty' <- generalize ty
  mapM_ ((flip addTag) ty') (map fst tags')
  addTypeVar name ty'
  return Fix{unFix = Term'VarDecl name tags `Typed` Type'None}

-- Typechecking record declarations is very similar to variant declarations, because records are
-- really just products of tagged fields and variants are just sums of tagged fields.
-- However, it also involves declaring projection functions.
typecheck Fix{unFix = Term'RcdDecl name fields} = do
  fields' <- mapM (mapM appTy) fields
  let ty = Type'Record name (M.fromList fields')
  ty' <- generalize ty
  addTypeVar name ty'
  -- declare projection functions for every field
  forM_ fields' $ \(field, fieldTy) -> do
    let projTy = Type'Abs [ty] fieldTy
    projTy' <- generalize projTy
    addVar field projTy'  
  return Fix{unFix = Term'RcdDecl name fields `Typed` Type'None}

-- check if variable exists, and then instantiate its type
typecheck Fix{unFix = Term'Var sym}
  | sym `elem` ["hd", "tl", "cons", "print"] =
      typecheck Fix{unFix = Term'Builtin (symToBuiltin sym)}
  | otherwise = do
      ty <- lookupVar sym >>= inst
      subs <- use substitutions
      return Fix{unFix = Term'Var sym `Typed` ty}

-- Typechecking fibers is very similar to typechecking abstractions (see below), with the added
-- requirements that (a) all the yield statements inside them must yield terms of the same type;
-- and (b) the body must end in a yield statement.
typecheck Fix{unFix = Term'Fiber ps env body} = do
  vars <- replicateM (length ps) freshTypeVar
  let bindings = M.fromList $ zip ps (map (Type'TypeAbs []) vars)
  (body', bodyTys) <- typecheckList body `withTempTermCtx` bindings
  -- throw a typeError if the last statement in the fiber body isn't a yield
  unless (isYield $ last body') $
    typeError "Fiber body must end in a yield statement"
  -- search for yield statements and make sure they yield the same yield type
  let yieldedTys = map getYieldedType (filter isYield body')
  retty <- unifyFold' yieldedTys
  subs <- use substitutions
  return Fix{unFix = Term'Fiber ps env body' `Typed` Type'Fiber (map (sub subs) vars) retty}
  where isYield Fix{unFix = Term'Yield _ `Typed` _} = True
        isYield _ = False
        getYieldedType Fix{unFix = Term'Yield Fix{unFix = _ `Typed` ty} `Typed` _} = ty
  
typecheck Fix{unFix = Term'Yield t} = do
  t' <- typecheck t
  return Fix{unFix = Term'Yield t' `Typed` Type'None}

-- `resume` statements are essentially just like function applications, except they require that
-- the abstraction being applied is a fiber instead of a function.
typecheck Fix{unFix = Term'Resume fiberName args} = do
  x <- freshTypeVar
  (fiber, fiberTy) <- typecheck' (Fix $ Term'Var fiberName)
  -- make sure the 'fiber' that we just lookup up and typechecked is actually a fiber
  case unFix fiber of
    _ `Typed` Type'Fiber _ _ -> return ()
    _ -> typeError "Expected fiber in resume statement"
  subs <- use substitutions
  ctx <- uses vars head
  let ctx' = M.map (sub subs) ctx
  (args', argtys) <- typecheckList args `withTempTermCtx` ctx'
  let fiberTy' = sub subs fiberTy
  subs' <- unify fiberTy' (Type'Fiber argtys x)
  let fiberTy'' = sub subs' fiberTy'
  substitutions %= (subs'++)
  return Fix{unFix = Term'Resume fiberName args' `Typed` sub subs' x}

-- create fresh type vars for the parameters, typecheck the body, and then infer the parameters
typecheck Fix{unFix = Term'Abs ps env body} = do
  -- new variables have no prefix
  vars <- replicateM (length ps) freshTypeVar
  let bindings = M.fromList $ zip ps (map (Type'TypeAbs []) vars)
  (body', bodyTys) <- typecheckList body `withTempTermCtx` bindings
  subs <- use substitutions
  let retty = sub subs (last bodyTys)
  -- Apply the substitutions to the body. This isn't important for the typechecker,
  -- but the compiler needs to know lots of type information.
  let body'' = map (subTyped subs) body'
      -- Apply the substitutions to the function parameter types (this *is* needed for the typechecker).
      vars' = map (sub subs) vars
  return Fix{unFix = Term'Abs ps env body'' `Typed` Type'Abs vars' retty}

-- builtin types are already known
typecheck Fix{unFix = Term'Builtin name} = do
  ty <- case name of
    -- (+), (-), (*), (/) : (int, int) -> int
    Builtin'Add -> return $ Type'Abs [Type'Int, Type'Int] Type'Int
    Builtin'Sub -> return $ Type'Abs [Type'Int, Type'Int] Type'Int
    Builtin'Div -> return $ Type'Abs [Type'Int, Type'Int] Type'Int
    Builtin'Mul -> return $ Type'Abs [Type'Int, Type'Int] Type'Int
    -- print : (str) -> <>
    Builtin'Prn -> return $ Type'Abs [Type'Str] (Type'Product [])
    -- hd : (['a]) -> 'a
    Builtin'Hd -> freshTypeVar >>= \x -> return (Type'Abs [Type'List x] x)
    -- tl : (['a]) -> ['a]
    Builtin'Tl -> freshTypeVar >>= \x -> return (Type'Abs [Type'List x] (Type'List x))
    -- cons : ('a, ['a]) -> ['a]
    Builtin'Cons -> freshTypeVar >>= \x -> return (Type'Abs [x, Type'List x] (Type'List x))
    Builtin'StrToList -> return $ Type'Abs [Type'Str] (Type'List Type'Char)
    Builtin'ListToStr -> return $ Type'Abs [Type'List Type'Char] Type'Str
  return Fix{unFix = Term'Builtin name `Typed` ty}

-- typecheck each part of the application, applying the new substitutions along the way
typecheck Fix{unFix = Term'AppAbs abs args} = do
  x <- freshTypeVar
  (abs', absty) <- typecheck' abs
  subs <- use substitutions
  ctx <- uses vars head
  let ctx' = M.map (sub subs) ctx
  (args', argtys) <- typecheckList args `withTempTermCtx` ctx'
  let absty' = sub subs absty
  subs' <- unify absty' (Type'Abs argtys x)
  substitutions %= (subs'++)
  let absty'' = sub subs' absty
      retty = sub subs' x
  return Fix{unFix = Term'AppAbs abs' args' `Typed` retty}

-- for products, just typecheck all of the other types inside
typecheck Fix{unFix = Term'Product ts} = do
  (ts', tys) <- typecheckList ts
  return Fix{unFix = Term'Product ts' `Typed` Type'Product tys}

-- unify all types inside a non-empty list
typecheck Fix{unFix = Term'List ts} = do
  (ts', ttys) <- typecheckList ts
  ty <- unifyFold' ttys
  return Fix{unFix = Term'List ts' `Typed` Type'List ty}

-- in terms of typing, sets work just like lists
typecheck Fix{unFix = Term'Set ts} = do
  (ts', ttys) <- typecheckList ts
  ty <- unifyFold' ttys
  return Fix{unFix = Term'Set ts' `Typed` Type'Set ty}

typecheck Fix{unFix = Term'Match matchee cases} = do
  let pats = map fst cases
  let results = map snd cases
  -- get an initial type for the matchee value
  (matchee', mty) <- typecheck' matchee
  -- create variable subontexts (with match variables) for the cases
  ctxes <- mapM bindMatchVars pats
  -- typecheck each pattern under its subcontext
  (pats', pattys) <- unzip `fmap` zipWithM (\p c -> typecheck' p `withTempTermCtx` c) pats ctxes
  -- unify the matchee type with all patterns, and collect the substitutions.
  unifyFold mty pattys
  -- compute the unified types of patterns and contexts
  subs <- use substitutions
  let pattys' = map (sub subs) pattys
  let ctxes' = map (M.map (sub subs)) ctxes
  -- typecheck every result branch under their respective match contexts
  (results', resultTys) <- unzip `fmap` zipWithM (\r c -> typecheckList r `withTempTermCtx` c)
                          results ctxes'
  -- unify the results via folding
  ty <- unifyFold' (map last resultTys)
  let cases' = zip pats' results'
  return Fix{unFix = Term'Match matchee' cases' `Typed` ty}

typecheck Fix{unFix = Term'Variant tag args} = do
  (args', argTys) <- typecheckList args
  ty <- lookupTag tag
  v@(Type'Variant name tags) <- inst ty
  case tags M.!? tag of
    Just paramTys -> do
      subs <- (unifyListsWith (unifyRec v v)) paramTys argTys
      substitutions %= (subs++)
      subs <- use substitutions
      return Fix{unFix = Term'Variant tag args' `Typed` sub subs v}
    Nothing -> typeError "Variant tag is member of the wrong type"

typecheck Fix{unFix = Term'Record name fields} = do
  -- typecheck each of the fields
  (typedFields, fieldTys) <- unzip `fmap` mapM typecheck' (M.elems fields)
  -- construct a type based off of these inferred types
  let ty = Type'Record name (M.fromList $ zip (M.keys fields) fieldTys)
  -- look up the type of the record by name
  actualTy <- lookupType (Type'TypeVar name) >>= inst
  -- try to unify the inferred type with the 'real' type
  subs <- unify ty actualTy
  substitutions %= (subs++)
  let ty' = sub subs ty
  return Fix{unFix = Term'Record name (M.fromList $ zip (M.keys fields) typedFields) `Typed` ty'}

typecheck Fix{unFix = Term'Pin t} = do
  (t', ty) <- typecheck' t
  return Fix{unFix = Term'Pin t' `Typed` ty}

-- if the term is anything else, throw an error
typecheck t = typeError $ "Unable to typecheck term " ++ prettyPrint t ++ "."

-- | Like @typecheck@, but returns the term and type in an easy-to-destructure tuple.
typecheck' :: Term -> MushroomTypecheck (TypedTerm, Type)
typecheck' t = typecheck t >>= \term@Fix{unFix = Typed _ ty} -> return (term, ty)

-- | Typecheck a list of terms and return them in a tuple with a list of their respective types.
typecheckList :: [Term] -> MushroomTypecheck ([TypedTerm], [Type])
typecheckList xs = fmap unzip (mapM typecheck' xs)

-- | Looks through a term and binds any variables inside it to "generic" types that can be anything.
-- Used for pattern matches.
bindMatchVars :: Term -> MushroomTypecheck TypeMap
bindMatchVars Fix{unFix = Term'Var sym} = do
  x <- Type'TypeAbs [] `fmap` freshTypeVar
  return (M.singleton sym x)
bindMatchVars Fix{unFix = Term'Product ts} = mconcat `fmap` mapM bindMatchVars ts
bindMatchVars Fix{unFix = Term'List ts} = mconcat `fmap` mapM bindMatchVars ts
bindMatchVars Fix{unFix = Term'Variant s ts} = mconcat `fmap` mapM bindMatchVars ts
bindMatchVars Fix{unFix = Term'Record name fields} = mconcat `fmap` mapM bindMatchVars (M.elems fields)
bindMatchVars _ = return M.empty
