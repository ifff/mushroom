module Mushroom.Typechecker.Unify
  ( unify
  , unifyRec
  , unifyFold
  , unifyFold'
  , unifyLists
  , unifyListsWith
  , generalize
  , inst
  , appTy
  , sub
  , subTyped ) where

import Control.Arrow (second)
import Control.Lens
import Control.Monad (foldM, join, replicateM, zipWithM, when)

import Data.Fix
import Data.Function (on)
import Data.List (groupBy)
import qualified Data.Map as M
import Data.Monoid (mconcat)
import qualified Data.Set as S

import Debug.Trace (trace)

import Mushroom.PrettyPrint
import Mushroom.Typechecker.Util
import Mushroom.Types

-- | Unifies two types by generating a substitution set that, when applied to each type,
-- would make the them equal.
unify :: Type -> Type -> MushroomTypecheck Substitutions
unify a b | a == Type'None || b == Type'None = typeError "Expected value, got declaration"
unify app@(Type'AppTypeAbs _ _) b = appTy app >>= \a -> unify a b
unify a app@(Type'AppTypeAbs _ _) = appTy app >>= \b -> unify a b
unify v@(Type'TypeVar sym) b = lookupType v >>= \a -> unify a b
unify a v@(Type'TypeVar sym) = lookupType v >>= \b -> unify a b
unify (Type'Placeholder s) t@(Type'Placeholder _) = return [(s, t)]
unify (Type'Placeholder s) t | s `notElem` freeTypeVars t = return [(s, t)]
unify t (Type'Placeholder s) | s `notElem` freeTypeVars t = return [(s, t)]
unify (Type'Product ss) (Type'Product ts) = unifyLists ss ts
unify (Type'List s) (Type'List t) = unify s t
unify (Type'Set s) (Type'Set t) = unify s t
unify v1@(Type'Variant n1 m1) v2@(Type'Variant n2 m2)
  | n1 == n2 && keys1 == keys2 = (unifyListsWith (unifyRec v1 v2)) vals1 vals2
  where keys1 = M.keys m1
        keys2 = M.keys m2
        vals1 = mconcat (M.elems m1)
        vals2 = mconcat (M.elems m2)
unify (Type'Record n1 m1) (Type'Record n2 m2)
  | n1 == n2 && keys1 == keys2  = unifyLists vals1 vals2
  where keys1 = M.keys m1
        keys2 = M.keys m2
        vals1 = M.elems m1
        vals2 = M.elems m2
unify (Type'Abs ps1 ret1) (Type'Abs ps2 ret2) = do
  s1 <- unifyLists ps1 ps2
  let ret1' = sub s1 ret1
      ret2' = sub s1 ret2
  s2 <- unify ret1' ret2'
  return (s2 ++ s1)
unify (Type'Fiber ps1 ret1) (Type'Fiber ps2 ret2) = do
  s1 <- unifyLists ps1 ps2
  let ret1' = sub s1 ret1
      ret2' = sub s1 ret2
  s2 <- unify ret1' ret2'
  return (s2 ++ s1)
unify a b
  | a == b = return []
  | otherwise = typeError $ prettyPrint a ++ " does not unify with " ++ prettyPrint b ++ "."

-- | Unifies types that occur within a recursive type. This function must be provided the two larger
-- types that the latter two types are a part of. It uses these types to unfold instances of
-- @Type'Self@ when the other type does not match.
unifyRec :: Type -> Type -> Type -> Type -> MushroomTypecheck Substitutions
unifyRec _ _ Type'Self Type'Self = return []
unifyRec ty1a ty2a Type'Self ty2b = unifyRec ty1a ty2a ty1a ty2b
unifyRec ty1a ty2a ty1b Type'Self = unifyRec ty1a ty2a ty1b ty2a
unifyRec _ _ ty1 ty2 = unify ty1 ty2

-- | Calculates the type obtained by unifying all types in a list together with a start value and
-- applying the resulting substitutions.
unifyFold :: Type -> [Type] -> MushroomTypecheck Type
unifyFold = foldM go
  where go ty1 ty2 = do
          subs <- unify ty1 ty2
          substitutions %= (subs++)
          return (sub subs ty1)

-- | Similar to @unifyFold@, but uses a new type variable for its start value.
unifyFold' :: [Type] -> MushroomTypecheck Type
unifyFold' tys = freshTypeVar >>= \x -> unifyFold x tys

-- | Unify a list of corresponding types. If there are substitutions from the same placeholder to
-- multiple types, this algorithm attempts to unify those types together
-- so that a bijective list of 'condensed' substitutions is returned.
unifyLists :: [Type] -> [Type] -> MushroomTypecheck Substitutions
unifyLists = unifyListsWith unify

unifyListsWith unifyFn l1 l2
  | length l1 == length l2 = do
      subs <- mconcat `fmap` zipWithM unifyFn l1 l2
      -- group substitutions for the same placeholders together
      let sameSubs = groupBy ((==) `on` fst) subs
          syms = sameSubs >>= map fst
      -- unify each substitution group together into one condensed substitution type
      tys <- mapM (unifyFold' . map snd) sameSubs
      -- make a condensed version of the substitutions
      let condensedSubs = zip syms tys
      -- remember to grab the new substitutions obtained during the folding
      newSubs <- use substitutions
      -- now return the condensed substitutions and the new ones
      return (condensedSubs ++ newSubs)
  | otherwise = typeError "Cannot unify types with different length"

-- | Finds the free type variables in the provided type.
freeTypeVars :: Type -> S.Set Symbol
freeTypeVars (Type'Placeholder sym) = S.singleton sym
freeTypeVars (Type'Abs ps ret) = foldr S.union (freeTypeVars ret) (map freeTypeVars ps)
freeTypeVars (Type'Fiber ps ret) = foldr S.union (freeTypeVars ret) (map freeTypeVars ps)
freeTypeVars (Type'TypeAbs ps ret) = freeTypeVars ret S.\\ S.fromList ps
freeTypeVars (Type'Product ts) = mconcat $ map freeTypeVars ts
freeTypeVars (Type'List t) = freeTypeVars t
freeTypeVars (Type'Set t) = freeTypeVars t
freeTypeVars (Type'Variant _ m) = mconcat $ map (mconcat . map freeTypeVars) params
  where params = M.elems m
freeTypeVars (Type'Record _ m) = mconcat (map freeTypeVars fieldTys)
  where fieldTys = M.elems m
freeTypeVars _ = S.empty

-- | Perform substitutions of [s => t] on a type.
sub :: Substitutions -> Type -> Type
sub subs t@(Type'Placeholder _) = foldr subOne t subs
  where subOne (s1, t1) t2@(Type'Placeholder s2) = if s1 == s2 then t1 else t2
        subOne s ty = sub [s] ty
sub subs (Type'Abs ps ret) = Type'Abs (map (sub subs) ps) (sub subs ret)
sub subs (Type'Fiber ps ret) = Type'Fiber (map (sub subs) ps) (sub subs ret)
sub subs t@(Type'TypeAbs ps ret) = Type'TypeAbs ps (sub subs' ret)
  where subs' = filter (\(s, _) -> s `notElem` ps) subs
sub subs (Type'Product ts) = Type'Product $ map (sub subs) ts
sub subs (Type'List t) = Type'List (sub subs t)
sub subs (Type'Set t) = Type'Set (sub subs t)
sub subs (Type'Variant name m) = Type'Variant name m'
  where m' = M.map (map (sub subs)) m
sub subs (Type'Record name m) = Type'Record name m'
  where m' = M.map (sub subs) m
sub _ t = t

-- | Perform substitutions on a @TypedTerm@'s type.
subTyped :: Substitutions -> TypedTerm -> TypedTerm
subTyped subs Fix{unFix = t `Typed` ty} = Fix{unFix = fmap (subTyped subs) t `Typed` sub subs ty}

-- | Instantiates a universal type, i.e., it fills in the type arguments with types.
inst :: Type -> MushroomTypecheck Type
inst (Type'TypeAbs ps ret) = do
  newPs <- replicateM (length ps) freshTypeVar
  let subs = zip ps newPs
  return $ sub subs ret
inst ty = typeError $ "Cannot instantiate type " ++ prettyPrint ty ++ "."

-- | If the provided type is a type application, it evaluates that application. Otherwise, it tries to
-- evalutate applications further down in the type.
appTy :: Type -> MushroomTypecheck Type
appTy (Type'AppTypeAbs abs args) = do
  abs' <- lookupType abs
  case abs' of
    -- TODO: you can factor this part out of @appTy@ and @inst@ (above)
    Type'TypeAbs ps ret -> do
      when (length ps /= length args) $
        typeError "Wrong number of arguments passed to type abstraction"
      let subs = zip ps args
      return $ sub subs ret
    _ -> typeError "Cannot apply a type that isn't a type application"
appTy v@(Type'TypeVar _) = lookupType v >>= inst >>= \ty -> appTy ty
appTy (Type'TypeAbs ps ret) = appTy ret >>= \ret' -> return (Type'TypeAbs ps ret')
appTy (Type'Product tys) = Type'Product `fmap` mapM appTy tys
appTy (Type'List ty) = Type'List `fmap` appTy ty
appTy (Type'Set ty) = Type'Set `fmap` appTy ty
appTy (Type'Abs ps ret) = do
  ps' <- mapM appTy ps
  ret' <- appTy ret
  return $ Type'Abs ps' ret'
appTy (Type'Fiber ps ret) = do
  ps' <- mapM appTy ps
  ret' <- appTy ret
  return $ Type'Abs ps' ret'
appTy (Type'Variant name tags) = mapM (mapM appTy) tags >>= \tags' -> return (Type'Variant name tags')
appTy (Type'Record name fields) = mapM appTy fields >>= \fields' -> return (Type'Record name fields')
appTy ty = return ty

-- | Does the opposite of @inst@ - it takes the free type variables in the argument and turns
-- the argument into a universal type with those free variable names as parameter bindings.
generalize :: Type -> MushroomTypecheck Type
generalize ty = do
  vars <- use typeVars
  let vars' = S.fromList . map fst . M.toList . head $ vars
      freeVars = freeTypeVars ty
      params = freeVars S.\\ vars'
      params' = S.toList params
  return $ Type'TypeAbs params' ty
