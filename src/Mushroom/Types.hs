{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell            #-}

-- | Declarations for many common types used throughout this codebase, as well as some utility
-- functions that work with those types.
module Mushroom.Types where

import Control.Applicative (Applicative)
import Control.Monad.Except (ExceptT, MonadError, throwError)
import Control.Monad.Fail (MonadFail, fail)
import Control.Monad.State

import Control.Lens (makeLenses)

import Data.Fix
import Data.Functor (Functor)
import qualified Data.Set as S
import Data.Set (Set)
import Data.Text (Text)
import qualified Data.Map.Strict as M

-- import qualified LLVM.AST as AST
-- import LLVM.AST.Instruction

type Symbol = String

type Term = Fix TermF
type TypedTerm = Fix TypedF

data TermF a =
  Term'IntLit Int
  | Term'FloatLit Float
  | Term'CharLit Char
  | Term'StrLit Text
  | Term'Var Symbol
  | Term'Pin a
  | Term'MacroDecl Symbol [(Symbol, SyntaxType)] (Maybe (Symbol, SyntaxType)) [a]
  | Term'Define Symbol a
  | Term'ModuleDecl Symbol [Symbol] [Symbol] [a]
  | Term'VarDecl Symbol [(Symbol, [Type])]
  | Term'RcdDecl Symbol [(Symbol, Type)]
  | Term'List [a]
  | Term'Set [a]
  | Term'Product [a]
  | Term'Variant Symbol [a]
  | Term'Record Symbol (M.Map Symbol a)
  | Term'Builtin Builtin
  | Term'Abs [Symbol] (M.Map Symbol TypedTerm) [a]
  | Term'Fiber [Symbol] (M.Map Symbol TypedTerm) [a]
  | Term'Yield a
  | Term'Resume Symbol [a]
  | Term'AppAbs a [a]
  -- cases are an alist because the order of them matters
  | Term'Match a [(a, [a])]
  | Term'AppMacro Symbol [SyntaxElement] (Maybe SyntaxElement)
  | Term'MacroSplice Symbol
  | Term'MacroSpliceList Symbol
  | Term'None -- ^ For terms that can't be used as values at all (e.g. @Term'Define@)
  deriving (Eq, Functor, Ord, Show)

data TypedF a = Typed (TermF a) Type
  deriving (Eq, Ord, Show)

-- | Recursively rips the types off of typed terms.
untypeTerm :: TypedTerm -> Term
untypeTerm Fix{unFix = Term'Define sym t `Typed` _} = Fix $ Term'Define sym (untypeTerm t)
untypeTerm Fix{unFix = Term'List ts `Typed` _} = Fix $ Term'List (untypeTerms ts)
untypeTerm Fix{unFix = Term'Set ts `Typed` _} = Fix $ Term'Set (untypeTerms ts)
untypeTerm Fix{unFix = Term'Product ts `Typed` _} = Fix $ Term'Product (untypeTerms ts)
untypeTerm Fix{unFix = Term'Variant sym ts `Typed` _} = Fix $ Term'Variant sym (untypeTerms ts)
untypeTerm Fix{unFix = Term'Record sym fields `Typed` _} = Fix $ Term'Record sym (untypeTerms fields)
untypeTerm Fix{unFix = Term'Abs ps env body `Typed` _} = Fix $ Term'Abs ps env (untypeTerms body)
untypeTerm Fix{unFix = Term'Fiber ps env body `Typed` _} = Fix $ Term'Fiber ps env (untypeTerms body)
untypeTerm Fix{unFix = Term'Yield t `Typed` _} = Fix $ Term'Yield (untypeTerm t)
untypeTerm Fix{unFix = Term'Resume sym args `Typed` _} = Fix $ Term'Resume sym (untypeTerms args)
untypeTerm Fix{unFix = Term'AppAbs abs args `Typed` _} = Fix $ Term'AppAbs (untypeTerm abs) (untypeTerms args)
untypeTerm Fix{unFix = Term'Match t cases `Typed` _} =
  Fix $ Term'Match (untypeTerm t) (map (\(k, ts) -> (untypeTerm k, untypeTerms ts)) cases)
-- TODO: macro decls and module decls
untypeTerm Fix{unFix = Term'MacroDecl _ _ _ _ `Typed` _} = undefined
untypeTerm Fix{unFix = Term'ModuleDecl _ _ _ _ `Typed` _} = undefined
-- I can't use a catch-all for the non-recursive terms here because the compiler doesn't feel good abt it
untypeTerm Fix{unFix = Term'IntLit a `Typed` _} = Fix (Term'IntLit a)
untypeTerm Fix{unFix = Term'FloatLit a `Typed` _} = Fix (Term'FloatLit a)
untypeTerm Fix{unFix = Term'StrLit a `Typed` _} = Fix (Term'StrLit a)
untypeTerm Fix{unFix = Term'CharLit a `Typed` _} = Fix (Term'CharLit a)
untypeTerm Fix{unFix = Term'Var a `Typed` _} = Fix (Term'Var a)
untypeTerm Fix{unFix = Term'Builtin a `Typed` _} = Fix (Term'Builtin a)
untypeTerm Fix{unFix = Term'VarDecl a b `Typed` _} = Fix (Term'VarDecl a b)
untypeTerm Fix{unFix = Term'RcdDecl a b `Typed` _} = Fix (Term'RcdDecl a b)
untypeTerm Fix{unFix = Term'AppMacro a b c `Typed` _} = Fix (Term'AppMacro a b c)
untypeTerm Fix{unFix = Term'MacroSplice a `Typed` _} = Fix (Term'MacroSplice a)
untypeTerm Fix{unFix = Term'MacroSpliceList a `Typed` _} = Fix (Term'MacroSpliceList a)
untypeTerm Fix{unFix = Term'None `Typed` _} = Fix Term'None
-- | A convenience function for mapping @untypeTerm@ across collections (and any functor, really).
untypeTerms :: Functor f => f TypedTerm -> f Term
untypeTerms = fmap untypeTerm

data Type =
  Type'Placeholder Symbol
  | Type'Int
  | Type'Float
  | Type'Char
  | Type'Str
  | Type'Self -- ^ Refers to the /outermost/ enclosing type.
  | Type'TypeVar Symbol
  | Type'TypeAbs [Symbol] Type -- ^ a type mapping type(s) to a type
  | Type'AppTypeAbs Type [Type]
  | Type'Product [Type]
  | Type'List Type
  | Type'Set Type
  | Type'Record Symbol (M.Map Symbol Type)
  | Type'Abs [Type] Type
  | Type'Fiber [Type] Type
  | Type'Variant Symbol (M.Map Symbol [Type])
  | Type'None -- ^ For terms that don't yield *any* value, not even unit (i.e. variable definitions)
  deriving (Eq, Ord, Show)

-- TODO:
-- ~ streamline builtin creation
-- ~ add set <-> list conversions
-- ~ add float operations
-- ~ add float <-> int conversions
-- | A datatype holding the different types of builtin operations available
data Builtin =
  Builtin'Add
  | Builtin'Sub
  | Builtin'Div
  | Builtin'Mul
  | Builtin'Prn
  | Builtin'Hd
  | Builtin'Tl
  | Builtin'Cons
  | Builtin'StrToList
  | Builtin'ListToStr
  deriving (Eq, Ord, Show)
 
-- | Translates a symbol to a builtin data constructor.
symToBuiltin :: String -> Builtin
symToBuiltin "+" = Builtin'Add
symToBuiltin "-" = Builtin'Sub
symToBuiltin "/" = Builtin'Div
symToBuiltin "*" = Builtin'Mul
symToBuiltin "print" = Builtin'Prn
symToBuiltin "hd" = Builtin'Hd
symToBuiltin "tl" = Builtin'Tl
symToBuiltin "cons" = Builtin'Cons
symToBuiltin "strToList" = Builtin'StrToList
symToBuiltin "listToStr" = Builtin'ListToStr

-- | Translates a  builtin data constructor to a symbol.
builtinToSym :: Builtin -> String
builtinToSym Builtin'Add = "+"
builtinToSym Builtin'Sub = "-"
builtinToSym Builtin'Div = "/"
builtinToSym Builtin'Mul = "*"
builtinToSym Builtin'Prn = "print"
builtinToSym Builtin'Hd = "hd"
builtinToSym Builtin'Tl = "tl"
builtinToSym Builtin'Cons = "cons"
builtinToSym Builtin'StrToList = "strToList"
builtinToSym Builtin'ListToStr = "listToStr"

-- | This type represents any element of Mushroom's syntax that can be passed around as an argument to a macro.
data SyntaxElement =
  Syntax'Decl Term
  | Syntax'Term Term
  | Syntax'Val Term
  | Syntax'Type Type
  | Syntax'Name Symbol
  | Syntax'List [SyntaxElement]
  deriving (Eq, Ord, Show)

data SyntaxType =
  SyntaxTy'Decl
  | SyntaxTy'Term
  | SyntaxTy'Val
  | SyntaxTy'Type
  | SyntaxTy'Name
  | SyntaxTy'List SyntaxType
  deriving (Eq, Ord, Show)

data ErrorType = TypeError | RuntimeError | CompileError | PreprocError
-- | A data type that holds info on a MushroomError.
data MushroomError = MushroomError
  { errorType :: ErrorType
  , errorMessage :: String }

instance Show MushroomError where
  show err = "[" ++ etype ++ " Error]: " ++ errorMessage err
    where etype = case errorType err of
            PreprocError -> "Preprocessor"
            TypeError -> "Type"
            RuntimeError -> "Runtime"
            CompileError -> "Compile"

newMushroomError :: ErrorType -> String -> MushroomError
newMushroomError = MushroomError

type CtxMap a = M.Map Symbol a
type CtxStack a = [CtxMap a]

-- | Extend a variable context map to include new bindings. If there are identical keys, the value
-- from the second map is chosen.
withCtx :: CtxMap a -> CtxMap a -> CtxMap a
withCtx = flip M.union

{---- PREPROCESSOR ----}
type MacroInfo = ([(Symbol, SyntaxType)], (Maybe (Symbol, SyntaxType)), [Term])
type MacroMap = CtxMap MacroInfo

data PreprocState = PreprocState
  { _macros :: MacroMap }

newPreprocState :: PreprocState
newPreprocState = PreprocState { _macros = M.empty }

makeLenses ''PreprocState

newtype MushroomPreproc a = MushroomPreproc
  { runPreproc :: ExceptT MushroomError (State PreprocState) a }
    deriving (Functor, Applicative, Monad, MonadState PreprocState,
              MonadError MushroomError)

instance MonadFail MushroomPreproc where
  fail = throwError . newMushroomError PreprocError

{---- TYPECHECKER ----}
type TypeMap = CtxMap Type
type TypeStack = CtxStack Type
type Relation = (Type, Type)
type Constraints = Set Relation
type Substitution = (Symbol, Type)
type Substitutions = [Substitution]

data TypecheckState = TypecheckState
  { _vars :: TypeStack -- ^ A stack of mappings from variables to their types.
  , _typeVars :: TypeStack -- ^ A mapping from type names to the type that they alias
  , _substitutions :: Substitutions -- ^ A collection of type substitutions produced from unification.
  -- | A counter for type variables, used to ensure that all placeholder names are unique.
  , _typeVarCount :: Int
  -- | A stack of mappings from variant tags to the types that they're a member of.
  , _tagMap :: TypeStack
  -- | A stack of the current module names (most immediate module = top of stack)
  , _moduleNames :: [Symbol] }

makeLenses ''TypecheckState

newTypecheckState :: TypecheckState
newTypecheckState = TypecheckState
  { _vars = [M.empty]
  , _typeVars = [M.empty]
  , _substitutions = []
  , _typeVarCount = 0
  , _tagMap = [M.empty]
  , _moduleNames = [] }

-- | The monad for typechecking.
newtype MushroomTypecheck a = MushroomTypecheck
  { runTypecheck :: ExceptT MushroomError (State TypecheckState) a }
  deriving (Functor, Applicative, Monad,
            MonadState TypecheckState, MonadError MushroomError)

instance MonadFail MushroomTypecheck where
  fail = throwError . newMushroomError TypeError

{---- INTERPRETER ----}
-- | A mapping from variable names to their values in stack form.
type BindingMap = CtxMap TypedTerm
type BindingStack = CtxStack TypedTerm
type Bindings = BindingStack

data InterpreterState = InterpreterState
  {  -- | A stack of variable bindings. Each level of the state represents a scope.
    _bindings :: Bindings }

makeLenses ''InterpreterState

newInterpreterState :: InterpreterState
newInterpreterState = InterpreterState
  { _bindings = [M.empty] }

-- | The monad that holds all the stuff for evaluating Mushroom programs.
newtype MushroomEval a = MushroomEval
  { runEval :: ExceptT MushroomError (StateT InterpreterState IO) a }
  deriving (Functor, Applicative, Monad, MonadState InterpreterState,
            MonadError MushroomError, MonadIO)

instance MonadFail MushroomEval where
  fail = throwError . newMushroomError RuntimeError

{---- COMPILER ----}
-- data CompilerState = CompilerState
--   { _localCount :: Int -- ^ A counter for the number of local variables declared
--   , _localDecls :: [Named Instruction] -- ^ A list of local declarations
--   -- | A list of top-level definitions
--   , _defs :: [AST.Definition] }

-- makeLenses ''CompilerState

-- newCompilerState :: CompilerState
-- newCompilerState = CompilerState
--   { _localCount = 0
--   , _localDecls = []
--   , _defs  = [] }

-- newtype MushroomCompile a = MushroomCompile
--   { runCompile :: ExceptT MushroomError (State CompilerState) a }
--   deriving (Functor, Applicative, Monad, MonadState CompilerState,
--             MonadError MushroomError)

-- instance MonadFail MushroomCompile where
--   fail = throwError . newMushroomError CompileError

------------------------------------------

-- | A type alias for the total state that's used when running a program.
type ProgramState = (PreprocState, TypecheckState, InterpreterState)
-- | An empty program state
newProgramState :: ProgramState
newProgramState = (newPreprocState, newTypecheckState, newInterpreterState)
