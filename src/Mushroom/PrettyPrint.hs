{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Mushroom.PrettyPrint (prettyPrint) where

import Data.Fix
import Data.Map.Strict (toList)
import Data.Text (unpack)
import qualified Text.Pretty as P
import Text.Pretty (Pretty(..))

import Mushroom.Types

-- | A function for pretty-printing things and then converting them into simple strings
prettyPrint :: (Pretty a, Show a) => a -> String
prettyPrint = show . pretty

-- Some simple functions that make pretty-printing easier.
angles :: P.Printer -> P.Printer
angles = P.wrap '<' '>'

list :: [P.Printer] -> P.Printer
list = P.sepByS (P.char ',')

delimList :: (P.Printer -> P.Printer) -> [P.Printer] -> P.Printer
delimList delim terms = delim $ list terms

instance Pretty SyntaxElement where
  pretty (Syntax'Decl t) = pretty t
  pretty (Syntax'Term t) = pretty t
  pretty (Syntax'Val  t) = pretty t
  pretty (Syntax'Type t) = pretty t
  pretty (Syntax'List ts) = delimList P.brackets (map pretty ts)

instance Pretty SyntaxType where
  pretty SyntaxTy'Decl = P.string "<decl>"
  pretty SyntaxTy'Term = P.string "<term>"
  pretty SyntaxTy'Val = P.string "<val>"
  pretty SyntaxTy'Type = P.string "<type>"
  pretty SyntaxTy'Name = P.string "<name>"
  pretty (SyntaxTy'List ty) = P.string "<[" <> pretty ty <> P.string "]>"

instance Pretty Term where
  pretty Fix{unFix = Term'IntLit x} = P.int x
  pretty Fix{unFix = Term'FloatLit x} = P.float x
  pretty Fix{unFix = Term'StrLit s} = P.string (show (unpack s))
  pretty Fix{unFix = Term'CharLit c} = P.quotes (P.char c)
  pretty Fix{unFix = Term'Var s} = P.string s
  pretty Fix{unFix = Term'Product ts} = delimList angles (map pretty ts)
  pretty Fix{unFix = Term'List ts} = delimList P.brackets (map pretty ts)
  pretty Fix{unFix = Term'Set ts} = P.string "#" <> delimList P.braces (map pretty ts)
  pretty Fix{unFix = Term'Abs _ _ _} = P.string "<abs>"
  pretty Fix{unFix = Term'Fiber _ _ _} = P.string "<fiber>"
  pretty Fix{unFix = Term'Builtin s} = P.string (builtinToSym s)
  pretty Fix{unFix = Term'Variant tag []} = P.string tag
  pretty Fix{unFix = Term'Variant tag ts} = P.string tag <> delimList angles (map pretty ts)
  pretty Fix{unFix = Term'Define sym val} = P.string ""
  pretty Fix{unFix = Term'Record name rcdData} = P.string ('%':name) <> delimList P.braces fields
    where fields = zipWith prettyField keys vals
          (keys, vals) = unzip (toList rcdData)
          prettyField k v = P.string (k ++ ": ") <> pretty v
  pretty t = P.string (show t)

instance Pretty (TermF (Fix TypedF)) where
  pretty (Term'IntLit x) = P.int x
  pretty (Term'FloatLit x) = P.float x
  pretty (Term'StrLit s) = P.string (show (unpack s))
  pretty (Term'CharLit c) = P.quotes (P.char c)
  pretty (Term'Var s) = P.string s
  pretty (Term'Product ts) = delimList angles (map pretty ts)
  pretty (Term'List ts) = delimList P.brackets (map pretty ts)
  pretty (Term'Set ts) = P.string "#" <> delimList P.braces (map pretty ts)
  pretty (Term'Abs _ _ _) = P.string "<abs>"
  pretty (Term'Fiber _ _ _) = P.string "<fiber>"
  pretty (Term'Builtin s) = P.string (builtinToSym s)
  pretty (Term'Variant tag []) = P.string tag
  pretty (Term'Variant tag ts) = P.string tag <> delimList angles (map pretty ts)
  pretty (Term'Define sym val) = P.string ""
  pretty (Term'Record name rcdData) = P.string ('%':name) <> delimList P.braces fields
    where fields = zipWith prettyField keys vals
          (keys, vals) = unzip (toList rcdData)
          prettyField k v = P.string (k ++ ": ") <> pretty v
  pretty t = P.string (show t)
  
instance Pretty TypedTerm where
  -- For typed terms, just show the outermost type
  pretty Fix{unFix = term `Typed` ty} = pretty term

instance Pretty Type where
  pretty (Type'Placeholder sym) = P.string sym
  pretty Type'Int = P.string "int"
  pretty Type'Float = P.string "float"
  pretty Type'Str = P.string "str"
  pretty Type'Char = P.string "char"
  pretty Type'Self = P.string "self"
  pretty (Type'TypeVar sym) = P.string sym
  pretty (Type'Product ts) = delimList angles (map pretty ts)
  pretty (Type'List ty) = P.brackets (pretty ty)
  pretty (Type'Set ty) = P.string "#" <> P.braces (pretty ty)
  pretty (Type'Abs params ret) = delimList P.parens (map pretty params)
                                 <> P.string " -> " <> pretty ret
  pretty (Type'Fiber params ret) = delimList P.parens (map pretty params)
                                   <> P.string " --> " <> pretty ret
  pretty (Type'Variant name tags) = P.string name <> tagList
    where tagList = delimList P.brackets $ map printTag (toList tags)
          printTag (tag, []) = P.string tag
          printTag (tag, params) = P.string tag <> delimList angles (map pretty params)
  pretty (Type'TypeAbs tys ty) = P.string "\8704" <> delimList angles
                                 (map pretty tys) <> P.string "." <> pretty ty
  pretty (Type'AppTypeAbs abs ps) = pretty abs <> delimList angles (map pretty ps)
  pretty (Type'Record name rcdData) = P.string ('%':name) <> delimList P.braces fields
    where fields = zipWith prettyField keys vals
          (keys, vals) = unzip (toList rcdData)
          prettyField k v = P.string (k ++ ": ") <> pretty v
  pretty ty = P.string (show ty)
