module Main where

import System.Environment (getArgs)

import MushroomRunner

main :: IO ()
main = getArgs >>= runMushroom
