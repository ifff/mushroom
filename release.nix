let pkgs = import <nixpkgs> {};
in {
  mushroom = pkgs.haskellPackages.callPackage ./mushroom.nix {};
}
